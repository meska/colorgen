"""
Random Color generator

example
cg = ColorGen()
color = cg.newcolor

based on: http://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/

"""

from random import random

class ColorGen:
    def __init__(self, s=0.5, v=0.95):
        """
        :param s: HSV Saturation
        :param v: HSV Value
        """
        self.golden_ratio = 0.618033988749895
        self.h = random()
        self.s = s
        self.v = v

    @property
    def newcolor(self):
        self.h += self.golden_ratio
        self.h %= 1
        return self.rgb_to_html(self.hsv_to_rgb(self.h, self.s, self.v))

    @staticmethod
    def rgb_to_html(rgb_tuple):
        hexcolor = '#%02x%02x%02x' % rgb_tuple
        return hexcolor

    @staticmethod
    def hsv_to_rgb(h, s, v):
        r, g, b = 1, 1, 1
        h_i = int(h * 6)
        f = h * 6 - h_i
        p = v * (1 - s)
        q = v * (1 - f * s)
        t = v * (1 - (1 - f) * s)
        if h_i == 0:
            r, g, b = v, t, p

        if h_i == 1:
            r, g, b = q, v, p
        if h_i == 2:
            r, g, b = p, v, t

        if h_i == 3:
            r, g, b = p, q, v

        if h_i == 4:
            r, g, b = t, p, v
        if h_i == 5:
            r, g, b = v, p, q

        return int(r * 256), int(g * 256), int(b * 256)
